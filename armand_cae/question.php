<?php 
	include 'database.php';
	include 'header1.php';
	include 'perfect_function.php';
?>
<?php  ?>
<?php
	//pang set ng number ng question para alam kung anong number na yung nag take ng quiz
	$number = (int) $_GET['n'];
	
	//para makuha yung total number of questions at maidisplay
	$query = "SELECT * FROM `questions`";

	$results = $mysqli->query($query) or die($mysqli->error.__LINE__);
	$total = $results->num_rows;
		
	// para malaman kung anong number na yung sinasagutan
	$query = "SELECT * FROM `questions`
				WHERE question_number = $number";
	$result = $mysqli->query($query) or die($mysqli->error.__LINE__);
	
	$question = $result->fetch_assoc();
	
	//pang kuha ng choices at para ma check
	$query = "SELECT * FROM `choices`
				WHERE question_number = $number";
	$choices = $mysqli->query($query) or die($mysqli->error.__LINE__);
?>
<!DOCTYPE html>
<html>
	<head>
	<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
	<header>
		<div class="container">
			<h1>Welcome to the Quiz</h1>
		</div>
		<hr>
	</header>
	<main>
		<div class="container">
			<div class="current">Question <?php echo $question['question_number']; ?> of <?php echo $total; ?></div>
			<p class="question">
				<?php echo $question['text']; ?>
			</p>
			<hr>
			<hr>
			<form method="post" action="process.php">
				<ul class="choices">
					<?php while($row = $choices->fetch_assoc()): ?>
						<li><input name="choice" type="radio" value="<?php echo $row['id']; ?>" /><?php echo $row['text']; ?></li>
					<?php endwhile; ?>
				</ul>
				<hr>
				<hr>
				<button type="submit" value="Submit" class="btn btn-primary"> Submit</button>
				<input type="hidden" name="number" value="<?php echo $number; ?>" />
			</form>
			<hr>
		</div>
	</main>
</body>
</html>