<?php
 include 'database.php';
 include 'header1.php';

?>
<?php
// para makuha yung total number ng question na query
 $query ="SELECT * FROM questions";
 $results = $mysqli->query($query) or die($mysqli->error.__LINE__);
 $total = $results->num_rows;
?>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="utf-8" />
	<title></title>
	<link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
	<header>
		<div class="container">
			<h1>Information Technology Quizzer</h1>
		</div>
	</header>
	<main>
		<div class="container">
			
			<ul>
				<li><strong>Number of Questions: </strong><?php echo $total; ?></li>
				<li><strong>Estimated Time: </strong><?php echo $total * .5; ?> Minutes</li>
			</ul>
			<form action="register.php">
			<button class="btn btn-success">Register</button>
			</form>
			

		</div>
	</main>
	
</body>
</html>