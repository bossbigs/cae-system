<?php include 'database.php'; ?>
<?php session_start(); ?>
<?php
	//Check to see if score is set_error_handler
	if(!isset($_SESSION['score'])){
		$_SESSION['score'] = 0;
	}
	
	if($_POST){
		$number = $_POST['number'];
		$selected_choice = $_POST['choice'];
		$next = $number+1;
		
		//pang kuha ng total number of questions para madisplay
		$query = "SELECT * FROM `questions`";
		//pang kuha ng result ng query
		$results = $mysqli->query($query) or die($mysqli->error.__LINE__);
		$total = $results->num_rows;
		
		
		// pang check kung tama ba yung choice na napili ng nag quiz
		
		$query = "SELECT * FROM `choices`
					WHERE question_number = $number AND is_correct = 1";
		$result = $mysqli->query($query) or die($mysqli->error.__LINE__);
		$row = $result->fetch_assoc();
		
		//para masettt yung tamang choiice
		$correct_choice = $row['id'];
		
		
		if($correct_choice == $selected_choice){
			$_SESSION['score']++;
		}

		//para ma check kung last na yung question tapos pupunta na siya sa final.php
		if($number == $total){
			header("Location: final.php");
			exit();
		} else {
			header("Location: question.php?n=".$next);
		}
	}