-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 01, 2020 at 03:16 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `armand_cae`
--

-- --------------------------------------------------------

--
-- Table structure for table `choices`
--

CREATE TABLE `choices` (
  `id` int(11) NOT NULL,
  `question_number` int(11) NOT NULL,
  `is_correct` tinyint(1) NOT NULL DEFAULT 0,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `choices`
--

INSERT INTO `choices` (`id`, `question_number`, `is_correct`, `text`) VALUES
(55, 1, 0, 'Armand Cortes'),
(56, 1, 1, 'Rodrigo Duterte'),
(57, 1, 0, 'Kieth Abuyuan'),
(58, 1, 0, 'Kelvin Marou Baliuag'),
(59, 0, 0, 'Armand'),
(60, 0, 1, 'Luigi'),
(61, 0, 0, 'Daniel'),
(62, 0, 0, 'Berto'),
(63, 2, 0, 'Armand'),
(64, 2, 1, 'Luigi'),
(65, 2, 0, 'Ed'),
(66, 2, 0, 'Berto'),
(67, 3, 1, 'Genesis'),
(68, 3, 0, 'Exodus'),
(69, 3, 0, 'Leviticus'),
(70, 3, 0, 'Numbers'),
(71, 4, 0, 'Victor Magtangol'),
(72, 4, 0, 'Unlce Berting'),
(73, 4, 0, 'Armand Cortes'),
(74, 4, 1, 'Leonardo da Vinci'),
(75, 5, 1, 'A famous drug lord'),
(76, 5, 0, 'A super hero '),
(77, 5, 0, 'Philippine president'),
(78, 5, 0, 'The man who cant be moved'),
(79, 6, 1, 'True'),
(80, 6, 0, 'False'),
(81, 7, 1, 'True'),
(82, 7, 0, 'False'),
(83, 8, 0, 'True'),
(84, 8, 1, 'False'),
(85, 9, 1, 'True'),
(86, 9, 0, 'False'),
(87, 10, 1, 'True'),
(88, 10, 0, 'False');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) NOT NULL,
  `question_number` int(11) NOT NULL,
  `text` varchar(555) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question_number`, `text`) VALUES
(8, 1, 'Who is the current president of the philippines?'),
(10, 2, 'What is the name of Mario s Brother?'),
(11, 3, 'What is the first book in the Old Testament?'),
(12, 4, 'Which artist painted the famous portrait painting Mona Lisa?'),
(13, 5, 'Who is Pablo Escobar?'),
(14, 6, 'Rubber duckies are made of rubber'),
(15, 7, 'SSD stands for solid state drive'),
(16, 8, 'RAM stands for Random Access Memorandum'),
(17, 9, 'Bill Gates didnt build his own gate'),
(18, 10, 'The end of a shoe lace is called aglet');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fullname`, `score`) VALUES
(4, 'Armand Paul A. Cortes Jr.', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `choices`
--
ALTER TABLE `choices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `choices`
--
ALTER TABLE `choices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
