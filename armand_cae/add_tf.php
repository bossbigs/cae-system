<?php
include "header.php";
?>
<!DOCTYPE html>
<html>
	<head>
	<title>True or False</title>
	<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
	<header>
		<div class="container">
			<h1>True or False</h1>
		</div>
	</header>
	<main>
		<div class="container">
        <hr>
			
			<?php
				if(isset($msg)){
					echo '<p>'.$msg.'</p>';
				}
			?>
			<form method="post" action="add_tf_proc.php">
				<p>
					<label>Question Number: </label>
					<input type="number" value="<?php echo $next; ?>" name="question_number" />
				</p>
                <hr>
                <hr>
				<p>
					<label>Question Text: </label>
					<input type="text" name="question_text" />
				</p>
				<p>
					<label>Choice #1: </label>
					<input type="text" name="choice1" value="True" readonly />
				</p>
				<p>
					<label>Choice #2: </label>
					<input type="text" name="choice2" value="False" readonly />
				</p>
                <hr>
                <hr> 
				<p>
					<label>Correct Choice Number: </label>
					<input type="number" name="correct_choice" min="1" max="2" />
				</p>
                <hr>
				<p>
					<button class="btn btn-primary" type="submit" name="submit" value="Submit" style="width:200px"> Add</button>
				</p>
               
			</form>
            <form action="add.php">
			<button class="btn btn-warning" style="width:200px">Back</button>
			</form>
            <hr>
		</div>
	</main>
	
</body>
</html>