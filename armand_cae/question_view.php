
<?php 
session_start();
error_reporting(0);
include "header.php";
include "perfect_function.php"; 
  
?>


<a href="add.php" class="btn btn-primary">
    ADD Question
</a> <br><br>

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h2 class="m-0 font-weight-bold text-primary">Question Information</h2>
                </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
<thead>
    <tr>
        <td>Question No.</td>
        <td>Question</td>
        <td>Delete</td>
    </tr>
</thead>


<tfoot>
<tr>
        <td>Question No.</td>
        <td>Question</td>
        <td>Delete</td>
    </tr>
</tfoot>
<tbody>

    <?php
    $table_name = "questions";
        //get all records from users table
        $user_data = get($table_name);
        //fetch result set and pass it to an array (associatve)
        foreach($user_data as $key => $row){
            $id = $row['id'];
            $question_number = $row['question_number'];
            $text = $row['text'];
           

        
    ?>
    
    <tr>
        <td> <?= $question_number ?> </td>
        <td> <?= $text ?> </td>
        
        <td>
       
        <a href = "question_delete_alert.php?id=<?=$id?>" class="btn btn-danger btn-circle btn-md">
            <i class="fas fa-trash"></i>
        </a>
        </td>
    </tr>
    
    <?php } ?>
    <tbody>

</table>
<a href="home.php"><button class="btn btn-warning">HOME</button></a>
        </div>
        </div>
        </div>

