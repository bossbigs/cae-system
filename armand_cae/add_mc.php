<?php
include "header.php";
?>
<!DOCTYPE html>
<html>
	<head>
	<link rel="stylesheet" href="style.css" type="text/css" />
</head>
<body>
	<header>
		<div class="container">
			<h1>Multiple Choice</h1>
		</div>
	</header>
	<main>
		<div class="container">
			<h2>Add a Question</h1>
			<?php
				if(isset($msg)){
					echo '<p>'.$msg.'</p>';
				}
			?>
			<form method="post" action="add_mc_proc.php">
				<p>
					<label>Question Number: </label>
					<input type="number" value="<?php echo $next; ?>" name="question_number" />
				</p>
				<hr>
				<hr>
				<p>
					<label>Question Text: </label>
					<input type="text" name="question_text" />
				</p>
				<p>
					<label>Choice #1: </label>
					<input type="text" name="choice1" />
				</p>
				<p>
					<label>Choice #2: </label>
					<input type="text" name="choice2" />
				</p>
				<p>
					<label>Choice #3: </label>
					<input type="text" name="choice3" />
				</p>
				<p>
					<label>Choice #4: </label>
					<input type="text" name="choice4" />
				</p>
				<hr>
				<hr>
				
				<p>
					<label>Correct Choice Number: </label>
					<input type="number" name="correct_choice" min="1" max="4" />
				</p>
				<hr>
				<p>
					<button class="btn btn-primary" type="submit" name="submit" value="Submit" style="width:200px"> Add</button>
				</p>
               
			</form>
            <form action="add.php">
			<button class="btn btn-warning" style="width:200px">Back</button>
			</form>
			<hr>
		</div>
	</main>
	
</body>
</html>